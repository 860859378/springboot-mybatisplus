package com.isoftstone;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.isoftstone.entity.po.User;
import com.isoftstone.mapper.UserMapper;
import com.isoftstone.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class Demo3ApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    UserService userService;

    @Test
    void contextLoads() {
    }

    @Test
    public void test() {
      User user = userService.queryByName();
        System.out.println(user);
    }

    @Test
    public void shouldFindUserByNameAndCity() {
        User user = userService.queryByName();
        System.out.println(user.toString());
    }
    @Test
    public void shouldFindUserByNameAndCity2() {
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(User::getName, "李");
        List<User> users = userMapper.selectList(lambdaQueryWrapper);
        users.forEach(System.out::println);
    }
    @Test
    public void shouldFindUserByNameAndCity3() {
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(User::getName, "李");
        List<User> users = userService.list(lambdaQueryWrapper);
        users.forEach(System.out::println);
    }
}
