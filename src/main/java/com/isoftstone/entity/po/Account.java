package com.isoftstone.entity.po;

import java.io.Serializable;

/**
 * (Account)实体类
 *
 * @author wanggang
 * @since 2024-07-05 14:08:35
 */
public class Account implements Serializable {
    private static final long serialVersionUID = -69136980429933319L;

    private Integer id;

    private String name;

    private String money;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

}


