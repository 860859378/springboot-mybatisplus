package com.isoftstone.common;


public enum ResultCodeEnum {

    SUCCESS(200, "成功"),
    FAIL(10000, "失败"),
    NO_LOGIN(401, "未登录"),
    NO_PERM(403, "权限不足"),
    USERNAME_EXIST(10100, "用户名重复");
    private final Integer code;
    private final String message;

    ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}


