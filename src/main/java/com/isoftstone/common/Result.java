package com.isoftstone.common;


public class Result<T> {

    private String status;
    private Integer code;
    private String message;
    private T data;

    private Result() {
    }

    public static <T> Result build(String status, Integer code, T data, String message) {
        Result result = new Result<>();
        result.setStatus(status);
        result.setCode(code);
        result.setData(data);
        result.setMessage(message);
        return result;
    }

    public static <T> Result ok(T data) {
        return build("success", ResultCodeEnum.SUCCESS.getCode(), data, ResultCodeEnum.SUCCESS.getMessage());
    }

    public static Result ok() {
        return build("success", ResultCodeEnum.SUCCESS.getCode(), null, ResultCodeEnum.SUCCESS.getMessage());
    }

    public static Result fail(ResultCodeEnum resultCodeEnum) {
        return build("error", resultCodeEnum.getCode(), null, resultCodeEnum.getMessage());
    }

    public static Result fail(String message) {
        return build("error", ResultCodeEnum.FAIL.getCode(), null, message);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}


