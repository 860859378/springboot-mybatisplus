package com.isoftstone.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isoftstone.entity.po.User;

/**
 * (User)表服务接口
 *
 * @author wanggang
 * @since 2024-07-05 14:08:44
 */
public interface UserService extends IService<User> {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User update(User user);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    User queryByName();

}

